
## Instalação

 - Faça o download do repositório.
 - Use o gerenciador de pacotes npm.
 - Dentro da pasta do projeto e dentro da pasta client (assessment-frontend >> public >> client), execute o npm install no terminal para instalar todas as dependências.

```
npm install
```

## Instruções

- Execute npm Start na pasta do projeto para iniciar a aplicação 

```
npm start
```

Em seguida, abra uma nova janela no terminal e execute npm start na pasta client (assessment-frontend >> public >> client)

```
npm start
```

## Sobre o projeto
- Para consumir a API, foi utilizado o Node Express.
- No desenvolvimento do front-End, foi utiltilizado o ReactJs, HTML5 e CSS3.
- Para chamadas da API no front-end, utilizou-se axios.
- Para o menu responsivo, foi utilizado bootstrap.
