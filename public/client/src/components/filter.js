import React, { Component } from "react";
import "../style/filter.css";
import "../style/home.css";
import "../style/responsive.css";


class Filter extends Component {

  render() {
    return (
      <div className="filter">
        <div className="filter-by">
          <h2>FILTRE POR</h2>
          <h3>CATEGORIAS</h3>
          <p>• Roupas</p>
          <p>• Sapatos</p>
          <p>• Acessórios</p>

          <h3>CORES</h3>
          <div className="colors">
            <div id="block-1"></div>
            <div id="block-2"></div>
            <div id="block-3"></div>
          </div>

          <h3>TIPO</h3>
          <p>• Corrida</p>
          <p>• Caminhada</p>
          <p>• Casual</p>
          <p>• Social</p>
        </div>
      </div>
    );
  }
}

export default Filter;
