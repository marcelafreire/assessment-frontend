import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../style/home.css";
import "../style/responsive.css";


class Header extends Component {
  componentDidMount() {
    this.props.getCategories();
    this.props.handleSearch();
  }

  render() {
    return (

      
      <div>
        <div className="login">
          <p>
            <b>
              <u>Acesse sua conta </u>
            </b>
            ou
            <b>
              <u> cadastre-se</u>
            </b>
          </p>
        </div>

        <header className="menu">
          <div>
            <img src="https://cdn.digitalks.com.br/wp-content/uploads/2015/01/logo_webjump.jpg" alt="logo"/>
          </div>
          <div>
            <input type="text" name="search" onChange={this.props.handleSearch}></input>
            <button>BUSCAR</button>
          </div>
        </header>

   <nav className="navbar navbar-expand-sm">
   <div className="menu-responsive">
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span className="btn">&#9776;</span>
  </button>
  <img className="logo-responsive" src="https://cdn.digitalks.com.br/wp-content/uploads/2015/01/logo_webjump.jpg"  alt="logo"/>



  </div>
  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav">
      <li className="nav-item active">
       <Link className="nav-link" to="/">PÁGINA INICIAL<span className="sr-only">(current)</span></Link>
      </li>
      {this.props.listOfCategories.map((item) => (
              <div key={item.id}>
                <Link to={`/categories/${item.id}`}>
                  <li className="nav-item">{item.name.toUpperCase()}</li>
                </Link>
              </div>
            ))}
      <li className="nav-item">
        <a className="nav-link" href="#">CONTATO</a>
      </li>
    </ul>
  </div>
</nav>


{/* 
            {this.props.listOfCategories.map((item) => (
              <div key={item.id}>
                <Link to={`/categories/${item.id}`}>
                  <li>{item.name.toUpperCase()}</li>
                </Link>
              </div>
            ))} */}
          

      </div>

    );
  }
}

export default Header;
