import React, { Component } from "react";
import axios from "axios";
import "../style/section.css";

class Section3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //   allCategories: [],
      category3: [],
    };
    this.getOneCategorie = this.getOneCategorie.bind(this);
  }

  componentDidMount() {
    this.props.getCategories();
    this.getOneCategorie();
  }

  getOneCategorie() {
    axios
    .get("/3")
    .then((response) => {
      // console.log(response, " RESPONSEEEEEE")
     this.setState({category3: response.data.items})})
    .catch((error) => console.log(error));
  
  }

  render() {
    // console.log(this.props.listOfCategories, 'all categories')
    return (
      <div className="categories-section">
        <h2>Calçados</h2>
        <div class="line">
          <img src="/media/th-list-solid.svg" alt="icon"/>
          <div className="orderBy">
            <p>ORDENAR POR</p>
            <select>
              <option value="volvo">Preço</option>
            </select>
          </div>
        </div>

        <div className="products">
          {this.state.category3.map((item) => {
            return (
              <div key={item.id} className="products-infos">
                <img src={`/${item.image}`} alt={item.name}></img>
                <h3>{item.name.toUpperCase()}</h3>
              <div className="price">
              {item.specialPrice && <p>R${item.specialPrice}</p>}
                <h4>R${item.price}</h4>
                </div>
                <button>COMPRAR</button>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Section3;
