import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import "../style/home.css";
import axios from "axios";
import Section1 from "./section-1";
import Section2 from "./section-2";
import Section3 from "./section-3";
import Header from "./header";
import Footer from "./footer";
import Filter from "./filter";
import Homepage from "./home-page"

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listOfCategories: [],
      search: ""
    };
    this.getCategories = this.getCategories.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  getCategories() {
    axios
      .get(`/list`)
      .then((response) => {
        // console.log(response, 'response')
        this.setState({
          listOfCategories: response.data.items,
        });
      })
      .catch((error) => console.log(error, "caiu no erro"));
  }

  componentDidMount() {
    this.getCategories();
  }

  handleSearch(e) {
    this.setState({
        search: e.target.value.toLowerCase()
      })
  }


  render() {
    // console.log(this.state.listOfCategories, 'categories')
    return (
      <div>
        <Header
          getCategories={this.getCategories}
          listOfCategories={this.state.listOfCategories}
          handleSearch={() => this.handleSearch}
          search={this.state.search}
        />

        <div className="filter-category">
          <Filter />

          <div className="categories">
            <Switch>

            <Route
                exact
                path="/"
                render={(props) => (
                  <Homepage
                    getCategories={this.getCategories}
                    listOfCategories={this.state.listOfCategories}
                    {...props}
                  />
                )}
              />


              <Route
                exact
                path="/categories/1"
                render={(props) => (
                  <Section1
                    getCategories={this.getCategories}
                    listOfCategories={this.state.listOfCategories}
                    {...props}
                  />
                )}
              />

              <Route
                exact
                path="/categories/2"
                render={(props) => (
                  <Section2
                    getCategories={this.getCategories}
                    listOfCategories={this.state.listOfCategories}
                    {...props}
                  />
                )}
              />

              <Route
                exact
                path="/categories/3"
                render={(props) => (
                  <Section3
                    getCategories={this.getCategories}
                    listOfCategories={this.state.listOfCategories}
                    {...props}
                  />
                )}
              />
            </Switch>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
