import React, { Component } from "react";
import "../style/home.css";
import axios from "axios";

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = {
            categories: []
        };
        this.getAllProducts = this.getAllProducts.bind(this)
      }
    
      getAllProducts() {
          const breakPoints = [1, 2, 3]
          breakPoints.map(item => { 
        return axios
        .get(`/${item}`)
        .then((response) => {
         this.setState({categories: response.data.items})})
        .catch((error) => console.log(error));
          })
      }
    
      componentDidMount() {
        this.getAllProducts()
      }

  render() {
    // const newProduct = [...this.state.categories]
    // console.log(newProduct, 'teste')
    return (
        <div className="categories-section">
        <h2>Home</h2>
        <div className="line">
          <img src="/media/th-list-solid.svg" alt="icon" />
          <div className="orderBy">
            <p>ORDENAR POR</p>
            <select>
              <option value="volvo">Preço</option>
            </select>
          </div>
        </div>

      <div className="products">
          {this.state.categories.map((item) => {
            return (
              <div key={item.id} className="products-infos">
                <img src={`/${item.image}`} alt={item.name}></img>
                <h3>{item.name.toUpperCase()}</h3>
              <div className="price">
              {item.specialPrice && <p>R${item.specialPrice}</p>}
                <h4>R${item.price}</h4>
                </div>
                <button>COMPRAR</button>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Homepage;
